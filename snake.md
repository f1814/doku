# Snake in Scratch
Eine kleine Anleitung, um Snake in Scratch zu programmieren. Den Scratch Editor findet ihr [hier](https://scratch.mit.edu/projects/editor/). 

## Was ist Scratch
Scratch ist eine Blockprogrammiersprache. Das heißt es muss kein textueller Code geschrieben werden, wie zum Beispiel in der Arduino IDE, sondern die Befehle werden in Form von Blöcken zur Verfügung gestellt. 
Die Programmierkonzepte, wie zum Beispiel Variablen, Schleifen, Bedingungen und Funktionen, sind allerdings dieselben.

Die Blöcke sind in verschiedene Rubriken eingeteilt, besonders sind dabei die letzten zwei: 

1. Variablen - hier können eigene Variablen erzeugt und Werte zugewiesen werden
2. Meine Blöcke - hier können eigene Blöcke erzeugt werden. Dabei wir dem Block erstmal nur ein Name zugewiesen. Diesem erzeugten Block kann dann Logik zugewiesen werden. Erzeugte Blöcke können ganz oft wiederverwendet werden, ohne das die Logik darin kopiert werden muss. In der textuellen Programmierung entspricht das dem Konzept von Funktionen.

### Speichern von Programmen
Um dein Programm zu speichern, gehst du auf _Datei -> Auf deinem Computer speichern_

![speichern](./images/speichern_scratch.png)

Wenn du das Programm wieder laden möchtest, klickst du auf _Datei -> Von deinem Computer hochladen_

## Schritte zur grundsätzlichen Programmierung

### 1. Erstelle eine Schlange

Um eine Schlange zu erzeugen, gehst du unten rechts im Feld _Figur_ auf das Icon mit der Katze und wählst den Pinsel aus. Achtung, erst klicken, wenn du mit der Maus auf dem Pinsel bist, sonst kommst du in die Auswahl fertiger Bilder. Jetzt kannst du einen Schlangenkopf malen. Achte dabei darauf, dass dieser sich auf der markierten Mitte befindet. Wie der Kopf aussieht ist dir überlassen, wichtig ist, dass er eine Zunge oder Ähnliches vorne hat. Wenn du im Feld _Figur_ auf den Schlangenkopf drückst, kannst du ihm oben rechts einen anderen Namen geben. In dieser Ansicht kannst du auch die Größe der Figur anpassen.

![malen](./images/malen_scratch.png)

![schlangenkopf](./images/schlangenkopf_scratch.jped)

### 2. Bewege die Schlange mittels der Pfeiltasten

Wir benutzen die Pfeiltasten zur Bewegung des Schlangenkopfes. Hierfür gehst du zu der Kategorie _Ereignisse_ und verwendest den Block _Wenn Taste ... gedrückt wird_. Dann gehst du in die Kategorie Bewegung und wählst den Block _setze Richtung auf ... Grad_. Wie geht es weiter? Wenn du fertig bist sollte sich die Schlange drehen, sobald du die Pfeiltasten drückst.

![pfeiltasten](./images/pfeiltasten_scratch.png)

### 3. Bewege die Schlange fort

Damit wir das Spiel starten können, wählen wir unter der Kategorie _Ereignisse_ den Block _Wenn "grüne Fahne" angeklickt wird_. Was muss jetzt getan werden damit sich die Schlange bewegt? Schaue dir dafür die Kategorien _Steuerung_ und _Bewegung_ an. Denk daran, dass eine Bewegung etwas ist, was die ganze Zeit wiederholt werden muss. In welche Richtung soll die Schlange beim Start schauen und wo soll sie sich befinden?

![start](./images/start_scratch.png)

### 4. Gib der Schlange einen Körper

Um einen Körper zu erstellen, gehst du wie in Punkt 1 beschrieben in das Zeichen Menü. Mache den Körper ungefähr gleichgroß wie den Schlangenkopf. Hierbei ist wichtig, dass der Körper mittig um die angezeigte Mitte platziert wird. Der Körper sollte eine andere Farbe als der Kopf haben.
Eventuell muss du die Größe und Position deines Körpers und deines Kopfes aufeinander abstimmen.

Um ein Skript für den Körper zu erstellen, muss dieser ausgewählt sein. Wir brauchen wieder ein Startereignis. Der Körper der Schlange soll die ganze Zeit dem Kopf folgen und sich selbst klonen. Hierfür brauchst du wieder die Kategorien _Steuerung_ und _Bewegung_.

Was eine geklonte Figur machen soll, kann unter Steuerung mit dem Block _Wenn ich als Klon entstehe_ programmiert werden. In unserem Fall möchten wir, dass der Klon eine Weile sichtbar ist und dann wieder verschwindet, dadurch sieht es aus, als würde sich der Körper der Schlange mitbewegen. Damit der Körper der Schlange in Abhängigkeit der Punkte größer wird, verwenden wir die Variable Punkte als dynamische Größe. Bevor du den Körper bewegen kannst, musst du unter der Kategorie _Variablen_ eine Variable _Punkte_ erstellen.

__Tipp__: Je größer der Wert ist durch den du die Punkte teilst, desto langsamer wächst deine Schlange später.

![Koerper](./images/koerper_scratch.png)

![Klon](./images/klon_scratch.png)

### 5. Erstelle Futter und vergebe Punkte
Damit wir Punkte sammeln können, brauchen wir Futter für die Schlange. Erzeuge eine neue Figur, diesmal kannst du direkt auf das Katzen-Icon klicken und dir ein Bild für dein Futter aussuchen.

Um ein Skript für das Futter zu erstellen, muss dieses wieder ausgewählt werden. Wähle einen Startblock und positioniere das Futter zufällig auf der Karte. Schaue dir dafür die Kategorien _Bewegung_ und _Operatoren_ an. 

__Tipp:__ 
* Die möglichen Werte für _x_ sind -220 bis 220. 
* Die möglichen Werte für _y_ sind -140 bis 140.

Nach der Positionierung muss fortlaufend geprüft (Kategorie _Steuerung_) werden, ob der Schlangenkopf das Futter berührt (Kategorie _Fühlen_). Wenn das passiert müssen die Punkte um 1 (Kategorie _Variablen_) hochgezählt und das Futter neu positioniert werden (Verwende die Positionierung vom Anfang wieder). 

Die Lösung könnte zum Beispiel so aussehen:

![Futter](./images/futter_scratch.png)

Jetzt sollte deine Schlange Futter einsammeln und immer länger werden können. Bei mir sieht das Spiel zum Beispiel so aus.

![Snake](./images/snake_scratch.png)

## Erweiterungen und Variationen

Das Spiel kann um immer mehr Regeln ergänzt werden. Hier sind einige Beispiele. Du kannst dir natürlich auch selbst etwas ausdenken.

### Lasse das Spiel aufhören, wenn die Schlange eine Wand berührt

Kategorien: _Fühlen_, _Steuerung_

### Spiele einen Sound und lasse die Schlange etwas sagen wenn das Spiel aufhört

Kategorien: _Klang_, _Aussehen_

### Lasse das Spiel aufhören, wenn die Schlange sich selbst berührt

Hier wird die Zunge deiner Schlange wichtig. Du kannst nicht wie beim Essen auf die Berührung der Figur prüfen, sondern musst stattdessen Farben verwenden.

Kategorien: _Fühlen_, _Steuerung_ 

### Lasse die Schlange durch Wände gehen

Die Schlange soll am Rand nicht einfach stehen bleiben, sondern am anderen Ende wieder herauskommen. Das ist etwas komplizierter. Hierfür musst du Wissen wie groß dein Spielfeld ist. Immer wenn der Kopf ein Ende berührt, muss die Position auf das jeweils entgegengesetzte Ende gesetzt werden.

### Tonausgabe für die Schlange

Lasse die Schlange bei Spielbeginn und Ende etwas über die Lautsprecher des Computers sagen. Schaue dir dafür die Erweiterungen unten links im Menü der Scratchumgeung an. Gibt es Blöcke die dir dabei helfen?

## Aufbau des ftDuino

Der ftDuino verfügt links und rechts unten über Eingänge und rechts oben über Ausgänge. Um die Aus- und Eingänge rechts verwenden zu können, muss eine externe Stromquelle angeschlossen werden. Die Eingänge links können ohne externe Stromquelle versorgt werden. In den Eingängen können zum Beispiel Taster oder andere Sensoren angeschlossen werden. Ausgänge können Aktoren wie Motoren oder Lichter mit Strom versorgen.

![ftDuino_Anschluesse](./images/anschluesse_ftduino.png)

## Allgemeines
```C++
// muss ganz oben im Dokument stehen, um die ftDuino Bibliotheken nutzen zu können
#include <FtduinoSimple.h>

// Setup Funktion, um die interne LED sowie den Serielle Monitor zu nutzen
void setup() {

  // initialisiere die interne LED als Ausgang
  pinMode(LED_BUILTIN, OUTPUT);
  
  // Starte den Seriellen Monitor und warte auf die USB-Verbindung
  Serial.begin(9600);
  while(!Serial);      
}

void loop() {
    // Ausgabe eines Wertes auf dem Seriellen Monitor
    Serial.println("Ich werde auf dem Seriellen Monitor angezeigt");

    // Warte eine halbe Sekunde
    delay(500);
}

```

## Zugriff auf den ftDuino

### Ansprache der Eingänge (links)

```C++
// 'I1' kann durch jeden anderen Eingang ersetzt werden, z.B. 'I2' 
ftduino.input_get(Ftduino::I1);
```

### Ansprache der Zählereingänge (rechts)
```C++
ftduino.counter_get_state(Ftduino::C1)
```

### Ansprache der Ausgänge (rechts)

```C++
// aktiviert den Strom auf Ausgang O1
ftduino.output_set(Ftduino::O1, Ftduino::HI);

// deaktiviert den Strom auf Ausgang O1, statt 'LO' kann auch 'OFF' geschrieben werden
ftduino.output_set(Ftduino::O1, Ftduino::LO);
```

### Ansprache der Ausgänge (rechts) als Motorausgänge

```C++
// lässt den Motor auf Ausgang M1 (O1+O2) nach links drehen
ftduino.motor_set(Ftduino::M1, Ftduino::LEFT);

// lässt den Motor auf Ausgang M1 (O1+O2) nach rechts drehen
ftduino.motor_set(Ftduino::M1, Ftduino::RIGHT);

// deaktiviert den Strom auf Ausgang M1 (O1+O2)
ftduino.motor_set(Ftduino::M1, Ftduino::OFF);
``` 

### Ansprache der internen LED

```C++
// lässt die LED leuchten
digitalWrite(LED_BUILTIN, HIGH);

// beendet das Leuchten der LED 
digitalWrite(LED_BUILTIN, LOW);
```

<div class="page"/>

## Grundsätzliche Programmierkonstrukte

### Variablen

Eine Variable speichert einen bestimmten Wert der später ausgelesen und/ oder verändert werden kann

```C++
// initialisiert die Variable
int photoWiderstand = 0;

// speichere den Wert von Eingang I2 in der Variable
photoWiderstand = ftduino.input_get(Ftduino::I2);
```

### Datentypen

Jede Variable kann bestimmte Arten von Daten speichern:

- `int` kann einen ganzzahligen Wert speichern (1, 2, 3, 4, ...). Mit Variablen dieses Typs kann gerechnet werden.
```C++
int v1 = 6;
int v2 = 3;

int v3 = v1 + v2 // v3 hat den Wert 9
int v4 = v1 * v2
int v5 = v1 - v2
int v6 = v1 / v2

```
- `float` kann Kommazahlen speichern (0.1, 0.2, 1.4, 1.5, ...). **Wichtig** Kommazahlen werden immer mit "." nicht wie gewohnt mit "," geschrieben. Auch mit Variablen dieses Typs kann gerechnet werden.
- `string` kann eine beliebige Zeichenkette speichern. Mit Variablen dieses Typs kann **nicht** gerechnet werden.

<div class="page"/>

### Schleifen

Eine Schleife (engl. loop) ist ein Code Block der so lange wiederholt wird, bis eine Abbruchbedingung eintritt.

```C++
// Wird auf Ausgang O1 eine Lampe und auf Eingang I2 ein Photowiderstand angeschlossen, blinkt die Lampe so lange es dunkel ist
photoWiderstand = ftduino.input_get(Ftduino::I2);

// wiederhole so lange, bis die Variable photoWiderstand einen anderen Wert als 0 annimmt
while (photoWiderstand == 0) {

    // aktiviere den Strom auf Ausgang O1
    ftduino.output_set(Ftduino::O1, Ftduino::HI);

    //wartet eine halbe Sekunde
    delay(500) 
    
    // deaktiviere den Strom auf Ausgang O1
    ftduino.output_set(Ftduino::O1, Ftduino::LO);
}
```

### Bedingungen

Eine Bedingung ist eine Verzweigung an der ein Wert abgefragt wird. Je nach Ausprägung des Werts wird der eine oder andere Code Block ausgeführt.

```C++
// Wird auf Eingang I1 ein Schalter angeschlossen, wird mit diesem Code Block auf dem Seriellen Monitor angezeigt, ob der Schalter gedrückt ist oder nicht.

// Prüfe, ob der Schalter gedrückt ist
if (ftduino.input_get(Ftduino::I1)) { 

    Serial.println("Der Schalter ist gedrückt");
    delay(1000)

} else {

    Serial.println("Der Schalter ist nicht gedrückt");
    delay(1000)

}
```



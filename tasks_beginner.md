<!-- markdownlint-disable MD033 -->

## Umfrage  

https://surveys.kompetenzz.de/gdc-ka-2/


# Eingabe und Ausgabe, Bedingungen und Schleifen

Ganz unten sind zwei Scratch Beispiele, die du dir anschauen kannst, wenn du nicht mehr weiter kommst

## Aufgabe 0

Schreibe ein Programm, das eine Zeile Text einliest und die Länge der Eingabe ausgibt

1. Eingabe: "Z" Ausgabe: "1"
2. Eingabe: "bbbbZ" Ausgabe: "5"
3. Eingabe: "ZZZ \_ZZ" Ausgabe: "7"
4. Eingabe: "ZZzZ" Ausgabe: "4"

<details>
  <summary>Tipp 1 (Scratch)</summary>
   Suche in den Operatoren
</details>

<details>
  <summary>Tipp 2 (Scratch)</summary>
  Benutze den "Länge von" Operator
</details>

<details>
  <summary>Tipp 1 (Java)</summary>
   Strings haben die Methode .length() recherchiere / teste wie man sie verwendet
</details>

## Aufgabe 0.5

Schreibe ein Programm, das eine Zeile Text einliest und den dritten Wert der Eingabe ausgibt
Was passiert wenn die Eingabe nur 2 Zeichen lang ist?
Für Java: Was für ein Datentyp hat der Wert an Stelle 3? Was passiert wenn die Eingabe eine Zahl ist?

<details>
  <summary>Tipp 1 (Scratch)</summary>
   Suche in den Operatoren
</details>

<details>
  <summary>Tipp 2 (Scratch)</summary>
  Benutze den "Zeichen x von" Operator
</details>

<details>
  <summary>Tipp 1 (Java)</summary>
   Strings haben die Methode .charAt(int) recherchiere / teste wie man sie verwendet
</details>

## Aufgabe 1

Schreiben ein Programm, das eine Zeile Text einliest und dann die Anzahl der Z in dieser Zeile ausgibt (also ermittelt, wie oft der Buchstabe Z in dieser Zeile vorkommt)

**Beispiele:**

1. Eingabe: "Z" Ausgabe: "1"
2. Eingabe: "ZZZZZ" Ausgabe: "5"
3. Eingabe: "ZZZaZZZ" Ausgabe: "6"
4. Eingabe: "ZZzZ" Ausgabe: "4"

<details>
  <summary>Tipp 1 (Java)</summary>
   Recherchiere was eine for Schleife ist, was ist anders als bei einer while Schleife? Wann benutze ich die eine, wann die andere?
</details>

## Aufgabe 2

Schreiben ein Programm, das eine Zeile Text einliest und dann die Anzahl der Z und Y in dieser Zeile ausgibt (also ermittelt, wie oft die Buchstaben Z und Y insgesamt in dieser Zeile vorkommt)
Wie oft musst du über die Eingabe gehen (iterieren)?

**Beispiele:**

1. Eingabe: "ZY" Ausgabe: "2"
2. Eingabe: "ZYZaYYZ" Ausgabe: "6"
3. Eingabe: "ZZYZ" Ausgabe: "4"

<details>
  <summary>Tipp 1 (Java)</summary>
   Recherchiere was eine for Schleife ist, was ist anders als bei einer while Schleife? Wann benutze ich die eine, wann die andere?
</details>

## Aufgabe 3

Schreibe ein Programm, welches eine Zahl einliest und dann ausgibt ob die Zahl gerade oder ungerade ist, recherchiere dazu, was **modulo** bedeutet. Wann ist eine Zahl gerade oder ungerade?

**Beispiele:**

1. Eingabe: "3" Ausgabe: "ungerade"
2. Eingabe: "15000" Ausgabe: "gerade"
3. Eingabe: "15001" Ausgabe: "ungerade"

## Aufgabe 4

Schreibe ein Programm, welches eine Zeile Text einliest und dann ausgibt ob die Länge der Zeile gerade oder ungerade ist
**Beispiele:**

1. Eingabe: "Hunger" Ausgabe: "gerade"
2. Eingabe: "AG" Ausgabe: "gerade"
3. Eingabe: "Ist es schwer, Programmieren zu lernen? Kurzgefasst lautet die Antwort: Nein. Aber es erfordert Zeit, Geduld und die richtige Unterstützung." Ausgabe: "gerade"

## Aufgabe 5 - Caesar Chiffre Start

Recherchiere was die Caesar Chiffre ist, versuche das Wort "Hallo" dem Schlüssel 5 mit Stift und Papier zu verschlüsseln.
Danach schreibe ein Programm, das dir die Stelle eines Buchstabens im Alphabet ausgibt.

<details>
  <summary>Tipp Scratch</summary>
  Definiere dir eine Variable die das ganze abc enthält
</details>

**Beispiele:**

1. Eingabe: "a" Ausgabe: "1"
2. Eingabe: "z" Ausgabe: "26"
3. Eingabe: "c" Ausgabe: "3"

## Aufgabe 6 - Caesar Chiffre 2

Schreibe ein Programm, welches einen Buchstaben mit dem Schlüssel 5 verschlüsselt.
Was passiert wenn du ein x verschlüsseln möchtest? Warum? Wie kannst du das Problem lösen?

**Beispiele:**

1. Eingabe: "a" Ausgabe: "f"
2. Eingabe: "g" Ausgabe: "l"

## Aufgabe 7 - Andere Sprachen

Du möchtest dich mit eurem englischen Austauschschüler unterhalten. Leider kannst du noch nicht so gut Englisch und schreibst deswegen ein Programm welches eine Eingabe erfragt und diese ins Englische übersetzt.
Schaue dir dafür die Erweiterungen links unten im Menü an. Gibt es Blöcke die dir dabei helfen können?

## Scratch Beispiele

Wenn du mal nicht weiterkommst, kannst du hier vielleicht Inspiration finden (für Aufgabe 1 und 2).

<details>
  <summary>Eingabe und Ausgabe mit Scratch</summary>
   <img alt="So hidden image!" src="./images/ein_ausgabe.png"/>
</details>

<details>
  <summary>Gib das dritte Zeichen einer Zeichenkette aus</summary>
   <img alt="So hidden image!" src="./images/index_string.png"/>
</details>

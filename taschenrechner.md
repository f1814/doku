## Taschenrechner programmieren
1. Schreibe ein einfaches Programm, dass die Zahlen 6 und 2 addiert und das Ergebnis auf dem seriellen Monitor ausgibt
2. Schreibe ein Programm, dass die Zahlen 6 und 2 nur addiert und das Ergebnis ausgibt, wenn ein Taster gedrückt wurde (Tipp: Wenn dein Ergebnis zu oft ausgegeben wird, füge ein Delay hinzu)
3. Erweitere dein Programm um weitere Rechenarten und Taster

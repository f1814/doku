# Kleine Programmieraufgaben

Folgende Aufgaben können mithilfe von z.B. Java und [BlueJ](https://bluej.org/) gelöst werden. Als Interaktion mit dem Nutzer wird die Konsole verwendet.

## Einlesen und Ausgeben

Schreibe ein Programm, welches Worte von der Tastatur einliest, bis das Wort *Ende* geschrieben wird. Danach sollen die Worte in derselben Reihenfolge wieder ausgegeben werden.

*Wichtige Konzepte*

- Funktionen
- Variablen
- Arrays
- Bedingungen (if ... else)
- Schleifen (while, for, foreach)
- Konsoleneingabe lesen und verarbeiten

*Beispiel zum Arbeiten mit der Konsole*

```Java
import java.util.Scanner;

Scanner scanner = new Scanner(System.in);
String stop = "Ende";

String userInput = scanner.next();

while(!userInput.equals(stop)) {

  System.out.println("input: " + userInput);
  userInput = scanner.next();

} 

scanner.close();
System.out.println("Das Programm wurde beendet.");
```

## Welcher Tag?

Schreibe ein Programm, welches zu einem bestimmten Datum den Wochentag ausgibt.

*Wichtige Konzepte* 

- Funktionen
- Variablen
- Arrays
- Bedingungen (if ... else)
- Schleifen (while, for, foreach)
- Konsoleneingabe lesen und verarbeiten
- Konvertierung von Datentypen
- Modulo Rechnung (Rechnung mit Rest)

*Tipps*

- Überlege dir vorher theoretisch
  - Was dein Programm können soll (Welche Texte und Eingaben sieht der Nutzer?)
  - Was musst du berechnen, wie könnte das funktionieren? (Denke an Schaltjahre!)
- Teile die Aufgabe auf und gehe schrittweise vor. Beginne zum Beispiel damit erstmal ein Datum von der Konsole einzulesen und wieder auszugeben.
- Verwende so viele Funktionen wie möglich wieder.
- Java hat verschiedene Bibliotheken die hilfreich sein können (z.B. someString.split("."))

*Wichtige Datentypen*

- double: eine Kommazahl, z.B. `3.5`
- int: eine ganze Zahl, z.B. `44`
- boolean: Wahrheitswert, kann `true` oder `false` sein
- char: ein einziges Zeiche z.B. `'a'`
- String: eine Zeichenkette, z.B. `"Hallo"`, `"35"`
- Array: ein Container mit vielen Objekten, hat eine feste Länge die bei Initialisierung angegeben werden muss, z.B. `String[] stringArr = new String[12];`
- ArrayList: ein Container mit vielen Objekten, hat eine dynamische Länge, z.B. `ArrayList<String> stringList = new ArrayList<String>();`

<details>
  <summary><b>Beispiele für Modulo Rechnung</b></summary>
  <code>

    import java.util.Scanner;

    Scanner scanner = new Scanner(System.in);
    int stop = 0;

    int zahlenInput = 100;

    System.out.println("Gib eine Zahl ein, diese wird Modulo 2 gerechnet. Gibst du 0 ein, wird das Programm beendet.");

    while(zahlenInput != stop) {
      zahlenInput = scanner.nextInt();
      
      System.out.println(zahlenInput + " mod 2 = " + (zahlenInput % 2));
    } 

    scanner.close();
    System.out.println("Das Programm wurde beendet.");

  </code>
</details>

## Namen aus Text suchen

Lade dir die Dateien [geschichte.txt](textFiles/geschichte.txt) und [names.txt](textFiles/names.txt) herunter und speichere sie auf deinem Desktop.
Schreibe nun ein Programm was die Datei geschichte.txt Zeile für Zeile einliest und das Resultat in einem String speichert. Versuche im Internet danach zu suchen, wie du eine Datei einlesen kannst.

Gibt die Geschichte auf der Konsole aus. Als nächstes sollen mithilfe der Datei names.txt alle Namen aus der Geschichte gesucht und ausgegeben werden.

<details>
  <summary><b>Tip1: Datei einlesen</summary>
  Das<b>Scanner</b> Object welches wir zum Einlesen von Eingaben aus der Konsole verwendet haben kann noch etwas mehr. Suche gezielt danach wie du damit etwas<b>zeilenweise</b> einlesen kannst.
</details>

<details>
  <summary><b>Tip2: Suchen in Strings</b></summary>
  Für strings gibt es die Methode <code> einString.contains(einTeilstring)</code>.
</details>

## Sudoku

Schreibe ein Programm welches ein Sudoku löst. Überlege dir dafür welche Datestrukturen du brauchst, um das Sudoku zu lösen. 

## Geheimschrift - Cäsar Chiffre

Cäsar hat laut Überlieferungen Texte verschlüsselt indem er jeden Buchstaben im Alphabet durch einen anderen ersetzt hat. Die Buchstaben haben dabei einen festen Abstand voneinander z.B. A -> F, B -> G, C -> H usw. Schreibe ein Programm welches einen normalen Text in das Cäsar Chiffre übersetzt und einen Cäsar Chiffre Text wieder zurück in einen normalen Text übersetzt. 
Lese die Eingaben jeweils von der Konsole ein und gib sie wieder auf der Konsole aus. 

Tipp1: Schreibe dir das "normale" Alphabet und das Cäsar Alphabet auf und versuche dir erst einmal zu überlegen wie das theoretisch möglich ist.

Tipp2: Denke bei der Umsetzung an Konzepte aus vorigen Aufgaben wie z.B. Modulo Rechnung. Modulo welcher Zahl wird in diesem Fall gerechnet?

## Geheimschrift - Variable Verschiebung

Sobald bekannt ist um wie viel verschoben wurde kann das Cäsar Chiffre relativ einfach geknackt werden. Ein anderer Ansatz ist eine zufällige Verschiebung z.B. in dem das heutige Datum mehrfach über einen Satz geschrieben wird. Die Zahl über dem Buchstaben bestimmt wie weit der jeweilige Buchstabe verschoben wird.

```
0106202201062022
DASISTEINTEXT

D -> D, A -> B, S -> S, I -> O, S ->U, usw.
```

Die Umsetzung ist ähnlich wie in der vorigen Aufgabe mit dem Unterschied, dass die Anzahl um die verschoben wird variabel ist.

## Geld wechseln

Du bist in einem Geschäft und möchtest bezahlen. Du hast Münzen und Scheine mit den folgenden Werten: 1, 2, 5, 10 und 50. 

- Schreibe ein Programm, welches als Eingabe die gewünschte Summe erfragt und als Ausgabe alle möglichen Kombinationen von Münzen mit denen die Summe erreicht werden kann. 
- Erweitere dein Programm so, dass immer die Kombination zurück gegeben wird, für die am wenigstens Münzen und Scheine benötigt werden.



